<?php
/**
 * Created by PhpStorm.
 * User: Benoti
 * Date: 15/07/15
 * Time: 16:58
 * text-domain: brozzme-product-navigation
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class WC_Settings_Tab_brozzme_product_nav_tab {

    /**
     * Bootstraps the class and hooks required actions & filters.
     *
     */
    public static function init() {
        
        add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
        add_action( 'woocommerce_settings_tabs_settings_tab_brozzme_product_navigation', __CLASS__ . '::settings_tab' );
        add_action( 'woocommerce_update_options_settings_tab_brozzme_product_navigation', __CLASS__ . '::update_settings' );

    }

    /**
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['settings_tab_brozzme_product_navigation'] = __( 'Navigation', 'brozzme-product-navigation' );
        return $settings_tabs;
    }

    public function _retrieve_product_taxonomies(){

        $post_type = 'product';

        $taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type ), 'objects');
        $registered_terms = '';
        foreach( $taxonomies as $taxonomy ) :
            $registered_terms[$taxonomy->name] = $taxonomy->labels->singular_name;
        endforeach;

        return $registered_terms;
    }
    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function settings_tab() {

        ?><p>Shortcode sample: [wc_bpn_navigation echo="false" position="right" same_tax="true" adjacent_tax="product_cat" nav_text="fa_font_only" navigation_container_css="wc-bpnav" title="Produits dans la même catégorie"]</p><?
        woocommerce_admin_fields( self::get_settings() );



    }


    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::get_settings()
     */
    public static function update_settings() {
        woocommerce_update_options( self::get_settings() );
    }


    /**
     * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
     *
     * @return array Array of settings for @see woocommerce_admin_fields() function.
     */
    public static function get_settings() {

        $settings = array(
            'section_title' => array(
                'name'     => __( 'Product navigation', 'brozzme-product-navigation' ),
                'type'     => 'title',
                'desc'     => __('Product navigation configuration.', 'brozzme-product-navigation'),
                'id'       => 'wc_settings_tab_brozzme_product_navigation_section_title'
            ),
            'enable_navigation' => array(
                'name' => __( 'Enable navigation', 'brozzme-product-navigation' ),
                'type' => 'select',
                'options'=> array('true' 	=> __('Yes','brozzme-product-navigation'),
                    'false'  	=> __('No','brozzme-product-navigation')
                ),
                'default'=> 'true',
                'desc' => '',
                'id'   => 'b_prod_nav_general_settings[enable_navigation]'
            ),
            'adjacent_in_same_tax' => array(
                'name' => __( 'Same taxonomy terms', 'brozzme-product-navigation' ),
                'type' => 'select',
                'options'=> array('true' 	=> __('Yes','brozzme-product-navigation'),
                    'false'  	=> __('No','brozzme-product-navigation'),
                ),
                'default'=> 'product_cat',
                'desc' => 'Wether to display post link of the same taxonomy term',
                'id'   => 'b_prod_nav_general_settings[adjacent_in_same_tax]'
            ),
            'adjacent_tax' => array(
                'name' => __( 'Taxonomy to follow', 'brozzme-product-navigation' ),
                'type' => 'select',
                'options' => WC_Settings_Tab_brozzme_product_nav_tab::_retrieve_product_taxonomies(),
                'default'=> 'product_cat',
                'desc' => '',
                'id'   => 'b_prod_nav_general_settings[adjacent_tax]'
            ),
            'navigation_position' => array(
                'name' => __( 'Navigation position', 'brozzme-product-navigation' ),
                'type' => 'select',
                'options'=> array('woocommerce_before_single_product' 	=> 'before_product',
                    'woocommerce_before_single_product_summary'  	=> 'before_summary',
                    'woocommerce_single_product_summary'	=> 'product_summary' ,
                    'woocommerce_after_single_product' 	=> 'after_product',
                    'woocommerce_after_single_product_summary'	=> 'after_summary'),
                'default'=> 'true',
                'desc' => '',
                'id'   => 'b_prod_nav_general_settings[navigation_position]'
            ),
            'navigation_float' => array(
                'name' => __( 'Navigation float', 'brozzme-product-navigation' ),
                'type' => 'select',
                'options'=> array('left' 	=> __('Left','brozzme-product-navigation'),
                                    'right'  	=> __('Right','brozzme-product-navigation')),
                'default'=> 'true',
                'desc' => '',
                'id'   => 'b_prod_nav_general_settings[navigation_float]'
            ),
            'navigation_text' => array(
                'name' => __( 'Navigation text', 'brozzme-product-navigation' ),
                'type' => 'select',
                'options'=> array('true'=>__('Text only', 'brozzme-product-navigation' ),
                                    'false'=>__('Icons', 'brozzme-product-navigation' ),
                                    'fa_font'=> __('Font Awesome symbol', 'brozzme-product-navigation'),
                                    'fa_font_only'=>__('No text with Fonawesome symbol','brozzme-product-navigation'),
                                    'icons_only'=>__('No text with icons','brozzme-product-navigation')),
                'default'=> 'true',
                'desc' => __( 'To use only text or with next and previous arrow', 'brozzme-product-navigation' ),
                'id'   => 'b_prod_nav_general_settings[navigation_text]'
            ),
            'fa_navigation_symbol' => array(
                'name' => __( 'Font Awesome Navigation symbol', 'brozzme-product-navigation' ),
                'type' => 'select',
                'options'=> array(
                    'default'=>__('Angle left and right', 'brozzme-product-navigation' ),
                    'cc'=>__('Chevron circle left and right', 'brozzme-product-navigation' ),
                    'arrow'=> __('Arrow', 'brozzme-product-navigation'),
                    'carretsquare'=>__('Carret square','brozzme-product-navigation'),
                    'step'=>__('Step backward and forward','brozzme-product-navigation'),
                    ),
                'default'=> 'default',
                'desc' => __( 'To use only text or with next and previous arrow.', 'brozzme-product-navigation' ),
                'id'   => 'b_prod_nav_general_settings[fa_navigation_symbol]'
            ),
            'fontawesome_size' => array(

                'name' => __( 'Font Awesome size', 'brozzme-product-navigation' ),
                'type' => 'text',
                'desc' => __( 'Default unit is rem.', 'brozzme-product-navigation' ),
                'id'   => 'b_prod_nav_general_settings[fontawesome_size]'

            ),
            'navigation_container_css' => array(

                    'name' => __( 'Additional Navigation container Css', 'brozzme-product-navigation' ),
                    'type' => 'text',
                    'desc' => __( 'Use it for styling and container position. Only one class, without dot.', 'brozzme-product-navigation' ),
                    'id'   => 'b_prod_nav_general_settings[navigation_container_css]'

            ),
            'section_end' => array(
                'type' => 'sectionend',
                'id' => 'wc_settings_tab_brozzme_product_navigation_section_end'
            )


        );


        return apply_filters( 'settings_tab_brozzme_product_navigation', $settings );
    }

}

WC_Settings_Tab_brozzme_product_nav_tab::init();

