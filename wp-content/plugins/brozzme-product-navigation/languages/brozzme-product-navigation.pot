# Copyright (C) 2016 Brozzme Product Navigation
# This file is distributed under the same license as the Brozzme Product Navigation package.
msgid ""
msgstr ""
"Project-Id-Version: Brozzme Product Navigation 1.2.0\n"
"Report-Msgid-Bugs-To: http://wordpress.org/support/plugin/brozzme-product-navigation\n"
"POT-Creation-Date: 2016-09-02 18:43:39+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-09-02 21:18+0100\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Last-Translator: Benoît Faure <dev@brozme.com>\n"
"Language-Team: Benoti <dev@brozzme.com>\n"
"Language: fr_FR\n"
"X-Generator: Poedit 1.7.4\n"

#: includes/brozzme_navigation_class.php:75
msgid "Previous"
msgstr "Précédent"

#: includes/brozzme_navigation_class.php:76
msgid "Next"
msgstr "Suivant"

#: includes/brozzme_product_navigation_widget_class.php:17
msgid "Add navigation to product page."
msgstr "Ajouter des boutons pour la navigation entre les produits."

#: includes/brozzme_product_navigation_widget_class.php:19
#: includes/wc-bpn-integration.php:88 includes/wc_bpn_settings.php:102
msgid "Product navigation"
msgstr "Navigation produit"

#: includes/brozzme_product_navigation_widget_class.php:45
msgid "You must set this widget within the <a href=\"admin.php?page=wc-settings&tab=settings_tab_brozzme_product_navigation\">Woocommerce settings / Navigation</a> panel."
msgstr "Vous devez régler cette zone widget dans le panneau de configuration <a href=\"admin.php?page=wc-settings&tab=settings_tab_brozzme_product_navigation\">Paramètres Woocommerce / Navigation</a>"

#: includes/wc-bpn-integration.php:36 includes/wc_bpn_settings.php:31
#: includes/wc_bpn_settings.php:45
msgid "Navigation"
msgstr "Navigation"

#: includes/wc-bpn-integration.php:90 includes/wc_bpn_settings.php:104
msgid "Product navigation configuration."
msgstr "Configuration Navigation produit"

#: includes/wc-bpn-integration.php:94 includes/wc_bpn_settings.php:108
msgid "Enable navigation"
msgstr "Activer la navigation en produit (automatique)"

#: includes/wc-bpn-integration.php:96 includes/wc-bpn-integration.php:106
#: includes/wc_bpn_settings.php:110 includes/wc_bpn_settings.php:121
msgid "Yes"
msgstr "Oui"

#: includes/wc-bpn-integration.php:97 includes/wc-bpn-integration.php:107
#: includes/wc_bpn_settings.php:111 includes/wc_bpn_settings.php:122
msgid "No"
msgstr "Non"

#: includes/wc-bpn-integration.php:104 includes/wc_bpn_settings.php:119
msgid "Same taxonomy terms"
msgstr "Dans les même catégories ou étiquettes"

#: includes/wc-bpn-integration.php:114 includes/wc_bpn_settings.php:130
msgid "Taxonomy to follow"
msgstr "Taxonomie à suivre"

#: includes/wc-bpn-integration.php:122 includes/wc_bpn_settings.php:139
msgid "Navigation position"
msgstr "Position de la navigation"

#: includes/wc-bpn-integration.php:134 includes/wc_bpn_settings.php:152
msgid "Navigation float"
msgstr "Ajustement de la position"

#: includes/wc-bpn-integration.php:136 includes/wc_bpn_settings.php:154
msgid "Left"
msgstr "Gauche"

#: includes/wc-bpn-integration.php:137 includes/wc_bpn_settings.php:155
msgid "Right"
msgstr "Droite"

#: includes/wc-bpn-integration.php:143 includes/wc_bpn_settings.php:162
msgid "Navigation text"
msgstr "Texte pour la navigation"

#: includes/wc-bpn-integration.php:145 includes/wc_bpn_settings.php:164
msgid "Text only"
msgstr "Texte seul"

#: includes/wc-bpn-integration.php:146 includes/wc_bpn_settings.php:165
msgid "Icons"
msgstr "Icônes"

#: includes/wc-bpn-integration.php:147 includes/wc_bpn_settings.php:166
msgid "Font Awesome symbol"
msgstr "Symboles FontAwesome"

#: includes/wc-bpn-integration.php:148 includes/wc_bpn_settings.php:167
msgid "No text with Fonawesome symbol"
msgstr "Pas de texte avec les symboles Fontawesome"

#: includes/wc-bpn-integration.php:149 includes/wc_bpn_settings.php:168
msgid "No text with icons"
msgstr "Pas de texte avec les icônes"

#: includes/wc-bpn-integration.php:151 includes/wc_bpn_settings.php:171
msgid "To use only text or with next and previous arrow"
msgstr "Pour utiliser le texte seul or avec les symboles de navigation"

#: includes/wc-bpn-integration.php:155 includes/wc_bpn_settings.php:175
msgid "Font Awesome Navigation symbol"
msgstr "Symbole Fontawesome"

#: includes/wc-bpn-integration.php:158 includes/wc_bpn_settings.php:178
msgid "Angle left and right"
msgstr "Angle gauche et droit"

#: includes/wc-bpn-integration.php:159 includes/wc_bpn_settings.php:179
msgid "Chevron circle left and right"
msgstr "Chevron dans un cercle"

#: includes/wc-bpn-integration.php:160 includes/wc_bpn_settings.php:180
msgid "Arrow"
msgstr "Flèche"

#: includes/wc-bpn-integration.php:161 includes/wc_bpn_settings.php:181
msgid "Carret square"
msgstr "Flèche dans un carré"

#: includes/wc-bpn-integration.php:162 includes/wc_bpn_settings.php:182
msgid "Step backward and forward"
msgstr "Avance et retour rapide"

#: includes/wc-bpn-integration.php:165 includes/wc_bpn_settings.php:186
msgid "To use only text or with next and previous arrow."
msgstr "Pour utiliser seulement les flèches de navigation."

#: includes/wc-bpn-integration.php:170 includes/wc_bpn_settings.php:191
msgid "Font Awesome size"
msgstr "Taille de la police Font Awesome"

#: includes/wc-bpn-integration.php:172 includes/wc_bpn_settings.php:193
msgid "Default unit is rem."
msgstr "Unité par défaut : rem."

#: includes/wc-bpn-integration.php:178 includes/wc_bpn_settings.php:199
msgid "Additional Navigation container Css"
msgstr "Conteneur de Navigation supplémentaire Css"

#: includes/wc-bpn-integration.php:180 includes/wc_bpn_settings.php:201
msgid "Use it for styling and container position. Only one class, without dot."
msgstr "Utilisez une class spécial pour la personnalisation et le positionnement du conteneur. Juste le nom de la class, sans le point."

#: includes/wc_bpn_settings.php:51 wc_brozzme_product_navigation.php:119
msgid "Settings"
msgstr "Réglages"

#: includes/wc_bpn_settings.php:52
msgid "Shortcode Help"
msgstr "Aide shortcode"

#: includes/wc_bpn_settings.php:78
msgid "Shortcode"
msgstr ""

#: includes/wc_bpn_settings.php:239
msgid "You may want to change button navigation behaviours. It is possible, when you add a shortcode into your content."
msgstr "Vous pourriez vouloir changer le comportement des boutons de navigation. C'est possible, quand vous ajouter un shortcode dans votre contenu."

#: includes/wc_bpn_settings.php:240
msgid "Don't forget to disable automatic button if you don't to display many navigation buttons."
msgstr "N'oubliez pas de désactiver l'insertion automatique des boutons si vous ne voulez pas en afficher plusieurs."

#: includes/wc_bpn_settings.php:241
msgid "The plugin options settings of the Settings section will stand for your default values if the shortcode miss one."
msgstr "Les paramètres des options de la section Paramètres feront office de valeur par défaut si une des valeurs manquent."

#: includes/wc_bpn_settings.php:242
msgid "The shortcode as it appears automatically:"
msgstr "Le shortcode tel qu'il apparait automatiquement :"

#: includes/wc_bpn_settings.php:244
msgid "You can add parameters:"
msgstr "Vous pouvez ajouter les paramètres :"

#: includes/wc_bpn_settings.php:247
msgid "depending on theme and shortcode area paste, if navigation displays before content set echo to false,"
msgstr "cela dépend du thème et la zone où le shortcode a été copié, si la navigation apparait avant le contenu, mettre écho sur false,"

#: includes/wc_bpn_settings.php:249
msgid "Possible values : true / false"
msgstr "Valeur possibles : true / false"

#: includes/wc_bpn_settings.php:253 includes/wc_bpn_settings.php:258
#: includes/wc_bpn_settings.php:261 includes/wc_bpn_settings.php:265
#: includes/wc_bpn_settings.php:269
msgid "Possible values:"
msgstr "Valeurs possibles :"

#: includes/wc_bpn_settings.php:254
msgid "This value will override the settings value, only for this shortcode."
msgstr "Cette valeur remplace les valeurs par défaut."

#: includes/wc_bpn_settings.php:269
msgid "any CSS class without the prefix dot"
msgstr "n'importe quel class CSS sans le préfixe (point)"

#: includes/wc_bpn_settings.php:273
msgid "if you want to display a title for your block, you can customize it with css pointing to previous \"navigation container css\" and h4 definitions."
msgstr "Si vous voulez afficher un titre pour votre bloc, vous pouvez le personnaliser avec la class CSS pointant sur la class choisie pour \"Conteneur CSS supplémentaire\" et h4."

#. Plugin Name of the plugin/theme
msgid "Brozzme Product Navigation"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://brozzme.com/woocommerce-product-navigation/"
msgstr ""

#. Description of the plugin/theme
msgid "Add navigation buttons to surf between product page."
msgstr "Ajoutez des boutons de navigations pour surfer entre les pages produits."

#. Author of the plugin/theme
msgid "Benoti"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://brozzme.com/"
msgstr ""
