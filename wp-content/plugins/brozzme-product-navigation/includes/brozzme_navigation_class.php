<?php


if (!defined('ABSPATH')) {
exit;
}

if (!class_exists('brozzme_navigation_class')) {

    class  brozzme_navigation_class{


        public function __construct()
        {

            $this->settings_options = get_option('b_prod_nav_general_settings');

            if($this->settings_options['enable_navigation']=='true'){
                $wc_action = (empty($this->settings_options['navigation_position'])) ? 'woocommerce_before_single_product' : $this->settings_options['navigation_position'];
                add_action($wc_action, array($this, 'wc_bpn_nav'), 10);

            }

            add_shortcode( 'wc_bpn_navigation', array($this, 'navigation_shortcode') );
        }

        public function wc_bpn_nav(){

            do_shortcode('[wc_bpn_navigation ]');
        }


        public function navigation_shortcode($atts){
            global $post;
            // verify that we are on a product page
            if(!is_singular() || is_front_page() || is_home() || is_archive() || is_tax() || is_404()
                    || $post->post_type != 'product'){
                return;
            }

            $atts = shortcode_atts(
                array(
                    'title' => '',
                    'echo'  => 'true',
                    'position' => (empty($this->settings_options['navigation_float']))? 'right' : $this->settings_options['navigation_float'],
                    'same_tax' => (empty($this->settings_options['adjacent_in_same_tax']))? 'true' : $this->settings_options['adjacent_in_same_tax'],
                    'tax_name' => (empty($this->settings_options['adjacent_tax']))? 'product_cat' : $this->settings_options['adjacent_tax'],
                    'nav_text' => (empty($this->settings_options['navigation_text']))? 'false' : $this->settings_options['navigation_text'],
                    'navigation_container_css' => (empty($this->settings_options['navigation_container_css']))? '' : $this->settings_options['navigation_container_css'],
                ), $atts, 'wc_bpn_navigation'
            );

            // Get the previous and next product links
            if($atts['same_tax'] == 'true'){
                $adjacent_tax = $atts['tax_name'];
                $previous_adjacent_post = get_adjacent_post(true,'',true, $adjacent_tax);
                $next_adjacent_post = get_adjacent_post(true,'',false, $adjacent_tax);

            }
            else{
                $previous_adjacent_post = get_adjacent_post(false,'',true);
                $next_adjacent_post = get_adjacent_post(false,'',false);
            }

            $previous_link = get_permalink($previous_adjacent_post->ID);
            $next_link = get_permalink($next_adjacent_post->ID);

            $previous_css_class = '';
            $next_css_class = '';
                if($atts['nav_text']=='false'){

                    $previous_css_class = 'bpn-arrow-';
                    $next_css_class = 'bpn-arrow-';
                }
            $previous_text = __('Previous','brozzme-product-navigation');
            $next_text = __('Next','brozzme-product-navigation');

            if($atts['nav_text'] == 'fa_font_only' or $atts['nav_text'] == 'icons_only'){
                $previous_text = '';
                $next_text = '';
            }
            $fa_class = '';
               if($atts['nav_text']== 'fa_font' or $atts['nav_text']=='fa_font_only'){
                   $fa_class = '-'.$this->settings_options['fa_navigation_symbol'];
               }

            $additional_class = $atts['navigation_container_css'];

            $content = '<div class="bpn-nav-container '.$additional_class.'" style="float:'.$atts['position'].';">';

            if($atts['title']!= ''){
                $content .= '<h4 >'.$atts['title'].'</h4>';
            }

            $content .= '<div class="'.$previous_css_class.'previous"><a href="'.$previous_link.'" class="fa-bpn'.$fa_class.'">'.$previous_text.'</a></div>
                            <div class="bpn-separator"></div>
                            <div class="'.$next_css_class.'next"><a href="'.$next_link.'" class="fa-bpn'.$fa_class.'">'.$next_text.'</a> </div>
                        </div>
                        <div class="clear"></div>';
            if($atts['echo'] == 'true'){
                echo $content;
            }
            else{
                return $content;
            }

        }

        public function print_additional_styles(){
            $option = get_option('b_prod_nav_general_settings');


            if($option['fontawesome_size']!=''){

                $fa_font_size = $option['fontawesome_size'];

                $array_unit = array('px', 'em', 'rem', '%', 'vh');
                foreach($array_unit as $unit){
                    $fa_font_stack = strpos($option['fontawesome_size'], $unit);
                    if($fa_font_stack !== false ){
                        $fa_font_size = explode($unit, $option['fontawesome_size']);
                        $fa_font_size = $fa_font_size[0];
                    }
                }

                ?><style>
                    .bpn-nav-container h4{
                        line-height: <?php echo $fa_font_size;?>rem;
                    }
                    .fa-bpn-default:after, .fa-bpn-default:before, .fa-bpn-cc:after, .fa-bpn-cc:before,
                    .fa-bpn-arrow:before, .fa-bpn-arrow:after, .fa-bpn-carretsquare:before, .fa-bpn-carretsquare:after,
                    .fa-bpn-step:before, .fa-bpn-step:after{
                        font-size: <?php echo $fa_font_size;?>rem;
                    }
                </style><?php
            }

        }
    }
}