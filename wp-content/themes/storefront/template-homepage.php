<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>

	<div id="home" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php do_action( 'homepage' ); ?>

<!-- 		    <div class="page-content">
		        <ul class='blog'>
			        <?php $the_query = new WP_Query( 'showposts=1' ); ?>
			        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
			        <li>
			        	<div class="image"><?php the_post_thumbnail(); ?></div>
			        	<div class="blog-text">
			        		<a href="<?php the_permalink() ?>" class="blog-title"><?php the_title(); ?></a>
				        	<div class="date"><?php the_date(); ?></div>
				        	<?php the_excerpt(); ?><span class="read_more"><a href="<?php the_permalink(); ?>">Read More</a></span>
			        	</div>
		        	</li>
			        <?php endwhile;?>
		        </ul> 
		        <div class="subSection">
		        	<h4>Subscribe</h4>
		        	<p>Want first dibs on my newest stuff?</p>
		        	<a href="wp-login.php?action=register" class="button">CLICK HERE</a>
		        </div>
		    </div> -->

<!-- 		    <div class="store-content">
					<?php do_action( 'storesection' ); ?>
				</div> -->

				<div class="page-content">
					<div class="title"><h1>Contact Me</h1></div>
					<p>Want to be among the first to learn about my latest work? <br />	
					Click on the box below.</p>
					<a href="contact/" class="button">Drop me a line</a>
				</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
